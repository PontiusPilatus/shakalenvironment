//
// Created by Egor Kashirin on 2019-08-18.
//

#ifndef DATASTRUCTURES_NODE_H
#define DATASTRUCTURES_NODE_H

#include <utility>
#include <stdexcept>

template<typename TYPE>
class Node {
    static_assert(std::is_copy_constructible<TYPE>::value
                  && std::is_copy_assignable<TYPE>::value
                  && std::is_move_assignable<TYPE>::value
                  && std::is_constructible<TYPE>::value);
public:
    Node() : _prev(0), _next(0) {}
    explicit Node(TYPE* data) : _prev(0), _data(data), _next(0) {}
    explicit Node(TYPE && data) {
        _data = new TYPE(data);
        _prev = 0;
        _next = 0;
    }
    explicit Node(TYPE data) {
        _data = new TYPE(data);
        _prev = 0;
        _next = 0;
    }
    Node(Node* next, TYPE && data, Node* prev)
        : _prev(prev)
        , _data(std::forward(data))
        , _next(next) {}
    Node(Node && node) noexcept
        :_prev(node._prev)
        , _data(node._data)
        , _next(node._next) {
        node._data = 0;
        node._next = 0;
        node._prev = 0;
    }
    Node(Node& node) = delete;
    Node(Node const* node) = delete;

    ~Node() = default;

    TYPE* data() const {
        return _data;
    }
    TYPE* data() {
        if (_data == nullptr) {
            throw std::out_of_range("Data is empty.");
        }
        return _data;
    }

    void data(TYPE && data) {
        _data = std::forward<TYPE>(data);
    }

    Node* next() const {
        return _next;
    }

    void next(Node* next) {
        _next = next;
    }

    Node* prev() const {
        return _prev;
    }

    void prev(Node* prev) {
        _prev = prev;
    }


private:
    TYPE*  _data;
    Node* _next;
    Node* _prev;
};


#endif //DATASTRUCTURES_NODE_H
