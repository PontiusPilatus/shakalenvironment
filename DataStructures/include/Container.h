//
//  Container.h
//  Project
//
//  Created by Egor Kashirin on 18/08/2019.
//

#ifndef Container_h
#define Container_h

#include <cstddef>
#include "IContainer.h"

template <typename TYPE>
class Container : public IContainer<TYPE> {
public:
    Container() : _size(0), _capacity(0){};

    ~Container() override = default;
protected:
    size_t _size;
    size_t _capacity;
};
#endif /* Container_h */
