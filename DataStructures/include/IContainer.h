//
//  IContainer.h
//  Project
//
//  Created by Egor Kashirin on 18/08/2019.
//

#ifndef IContainer_h
#define IContainer_h


#include <cstddef>

template <typename TYPE>
class IContainer {
public:
    IContainer() = default;
    virtual ~IContainer() = default;
    
    // Item Management
    
    /**
     Push object on selected index

     @param index place to push
     @param object data
     */
    virtual void push(size_t index, TYPE && object) = 0;

    /**
     Return top element of the container

     @return const first object
     */
    virtual TYPE top() const = 0;
    
    /**
     Removes element on the given position

     @param index position
     */
    virtual void remove(size_t index) = 0;
};

#endif /* IContainer_h */
