//
// Created by Egor Kashirin on 2019-08-27.
//

#ifndef ECO_SYSTEM_LIST_ITERATOR_H
#define ECO_SYSTEM_LIST_ITERATOR_H

#include "../Atoms/Node.h"

template<typename TYPE>
class List_Iterator {
protected:
    List_Iterator() = default;
    virtual ~List_Iterator() = default;

    explicit List_Iterator(Node<TYPE>* current_node, Node<TYPE>* head)
    : _current_node(current_node)
    , _list_head(head) {}

    Node<TYPE>* _current_node;
    Node<TYPE>* _list_head;

    void increment() {

        if (_current_node->next() == nullptr) {
            throw std::out_of_range("Out of range.");
        }
        _current_node = _current_node->next();
    }

    void decrement() {

        if (_current_node == _list_head) {
            throw std::out_of_range("Out of range.");
        }
        auto* current = _list_head;
        auto* previous = _list_head;

        while(current != _current_node) {
            previous = current;
            current = current->next();
        }

        _current_node = previous;
    }
};


#endif //ECO_SYSTEM_LIST_ITERATOR_H
