//
// Created by Egor Kashirin on 2019-08-18.
//

#ifndef DATASTRUCTURES_LIST_H
#define DATASTRUCTURES_LIST_H

#include <stdexcept>
#include <iterator>
#include "IContainer.h"
#include "Atoms/Node.h"
#include "Iterators/List_Iterator.h"

template<typename TYPE>
class List : IContainer<TYPE> {
public:
    List();
    List(std::initializer_list<TYPE> items);

    ~List() override;

    void push(size_t index, TYPE && object) override;
    void push_back(TYPE && object);

    TYPE head() const;
    TYPE top() const override;

    void remove(size_t index) override;

    [[nodiscard]] size_t size() const;

    bool operator==(List<TYPE> const& rhs) const;
    TYPE operator[](size_t index);
    TYPE operator[](size_t index) const;

    class const_iterator;
    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    using const_reverse_iterator = std::reverse_iterator<const_iterator>;
    const_reverse_iterator rbegin() const noexcept;
    const_reverse_iterator rend()   const noexcept;

    class iterator;
    iterator begin() noexcept;
    iterator end() noexcept;

    using reverse_iterator = std::reverse_iterator<iterator>;
    reverse_iterator rbegin() noexcept;
    reverse_iterator rend() noexcept;

#ifdef DEBUG
    // Just For Google Tets
    void PrintTo(::std::ostream* os) {
        Node<TYPE>* current = _head;
        os << _head->data();
        while(current->next() != null) {
            current = _head->next();
            os << current->data();
        }
    }
#endif
private:
    Node<TYPE>* _head;
    size_t _size;

    Node<TYPE>* iterate_through(size_t index) const;
};


template<typename TYPE>
List<TYPE>::List() : _size(0), _head(0) {}

template<typename TYPE>
List<TYPE>::List(std::initializer_list<TYPE> items) {
    // TODO move constructor?
    _size = 0;
    auto it = items.begin();
    _head = new Node<TYPE>(*it);
    ++it; ++_size;
    Node<TYPE>* current = _head;

    for (; it != items.end(); ++it) {
        current->next(new Node<TYPE>(*it));
        current = current->next();
        ++_size;
    }
}

template<typename TYPE>
List<TYPE>::~List() {
    Node<TYPE>* current = _head;
    while(_head != nullptr) {
        _head = _head->next();
        delete current;
        current = _head;
    }
}

template<typename TYPE>
void List<TYPE>::push(size_t index, TYPE && object) {
    if (_size < index) {
        throw std::out_of_range("Index can't be grater than size of the container...");
    }

    if (index == 0) {
        auto* new_head = new Node<TYPE>(object);
        new_head->next(_head);
        _head = new_head;
        ++_size;
        return;
    }

    Node<TYPE>* current = iterate_through(index);
    auto* new_node = new Node<TYPE>(object);

    new_node->next(current->next());
    current->next(new_node);
    ++_size;
}

template<typename TYPE>
void List<TYPE>::push_back(TYPE && object) {

    if (_size == 0) {
        auto* new_head = new Node<TYPE>(object);
        new_head->next(_head);
        _head = new_head;
        ++_size;
        return;
    }
    Node<TYPE>* current = iterate_through(_size);
    Node<TYPE>* new_node = new Node(object);

    new_node->next(nullptr);
    current->next(new_node);
    ++_size;
}

template<typename TYPE>
TYPE List<TYPE>::top() const {
    if (_head == nullptr) {
        throw std::out_of_range("Head is empty.");
    }
    return *(_head->data());
}

template<typename TYPE>
TYPE List<TYPE>::head() const {
    if (_head != nullptr) {
        throw std::runtime_error("Empty Head Node.");
    }
    if (_head->data() != nullptr) {
        throw std::runtime_error("Empty Node data.");
    }

    return *(_head->data());
}

template<typename TYPE>
void List<TYPE>::remove(size_t index) {
    if (_size <= index) {
        throw std::out_of_range("Index can't be grater than size of the container...");
    }

    if (index == 0) {
        Node<TYPE>* prev_head = _head;
        _head = _head->next();
        delete prev_head;
        --_size;
        return;
    }

    Node<TYPE>* current = iterate_through(index);

    Node<TYPE>* remove_node = current->next();

    current->next(remove_node->next());
    --_size;
    delete remove_node;
}

template<typename TYPE>
size_t List<TYPE>::size() const {
    return _size;
}

template<typename TYPE>
bool List<TYPE>::operator==(const List<TYPE> &rhs) const {
    if (rhs.size() != _size) {
        return false;
    }
    Node<TYPE>* self_head = _head;
    Node<TYPE>* rhs_head = rhs._head;

    for (size_t i = 0; i < _size; ++i) {
        if (*(self_head->data()) != *(rhs_head->data())) {
            return false;
        }
        self_head = self_head->next();
        rhs_head  = rhs_head->next();
    }

    return true;
}

template<typename TYPE>
TYPE List<TYPE>::operator[](size_t index) {
    if (_size <= index) {
        throw std::out_of_range("Index can't be grater than size of the container...");
    }

    Node<TYPE>* current = iterate_through(index+1);
    return *(current->data());
}

template<typename TYPE>
TYPE List<TYPE>::operator[](size_t index) const {
    if (_size <= index) {
        throw std::out_of_range("Index can't be grater than size of the container...");
    }

    Node<TYPE>* current = iterate_through(index+1);
    return *(current->data());
}

template<typename TYPE>
Node<TYPE> *List<TYPE>::iterate_through(size_t index) const {
    Node<TYPE>* current = _head;

    for(size_t i = 1; i < index; ++i) {
        current = current->next();
    }
    return current;
}


template<typename TYPE>
class List<TYPE>::const_iterator : protected List_Iterator<TYPE>, public std::iterator<std::bidirectional_iterator_tag, const TYPE, std::ptrdiff_t> {
public:
    const_iterator() = default;
    explicit const_iterator(Node<TYPE>* current_node, Node<TYPE>* head)
            : List_Iterator<TYPE>(current_node, head) {}

    const const_iterator& operator++() {
        this->increment();
        return *this;
    }
    const const_iterator operator++(int) {
        const_iterator temp_iter = *this;

        this->increment();
        return temp_iter;
    }
    const const_iterator& operator--() {
        this->decrement();
        return *this;
    }

    const const_iterator operator--(int) {
        const const_iterator temp_iter = *this;

        this->decrement();
        return temp_iter;
    }

    bool operator==(typename List<TYPE>::const_iterator const& it) const {
        return *(*this) == *it;
    }
    bool operator!=(typename List<TYPE>::const_iterator const& it) const {
        return !(*this == it);
    }

    TYPE& operator*() const { return *(this->_current_node->data()); }
    Node<TYPE>* operator->() const { return this->_current_node; }
};

template<typename TYPE>
typename List<TYPE>::const_iterator List<TYPE>::begin() const noexcept { return const_iterator(_head, _head); }

template<typename TYPE>
typename List<TYPE>::const_iterator List<TYPE>::end() const noexcept {
    auto* end = iterate_through(_size);
    return const_iterator(end, _head);
}

template<typename TYPE>
typename List<TYPE>::const_reverse_iterator List<TYPE>::rbegin() const noexcept { return const_reverse_iterator(end()); }

template<typename TYPE>
typename List<TYPE>::const_reverse_iterator List<TYPE>::rend() const noexcept { return const_reverse_iterator(begin()); }


template<typename TYPE>
class List<TYPE>::iterator : public List_Iterator<TYPE>, public std::iterator<std::bidirectional_iterator_tag, TYPE, std::ptrdiff_t> {
public:
    iterator() = default;
    explicit iterator(Node<TYPE>* current_node, Node<TYPE>* head)
            : List_Iterator<TYPE>(current_node, head) {}

    iterator& operator++() {
        this->increment();
        return *this;
    }
    iterator operator++(int) {
        iterator temp_iter = *this;

        this->increment();
        return temp_iter;
    }
    iterator& operator--() {
        this->decrement();
        return *this;
    }

    iterator operator--(int) {
        const iterator temp_iter = *this;

        this->decrement();
        return temp_iter;
    }

    bool operator==(typename List<TYPE>::iterator const& it) const {
        return *(*this) == *it;
    }
    bool operator!=(typename List<TYPE>::iterator const& it) const {
        return !(*this == it);
    }

    TYPE& operator*() const { return *(this->_current_node->data()); }
    TYPE& operator*() { return *(this->_current_node->data()); }
    Node<TYPE>* operator->() const { return this->_current_node; }
    Node<TYPE>* operator->() { return this->_current_node; }
};

template<typename TYPE>
typename List<TYPE>::iterator List<TYPE>::begin() noexcept { return iterator(_head, _head); }

template<typename TYPE>
typename List<TYPE>::iterator List<TYPE>::end() noexcept {
    auto* end = iterate_through(_size);
    return iterator(end, _head);
}

template<typename TYPE>
typename List<TYPE>::reverse_iterator List<TYPE>::rbegin() noexcept { return reverse_iterator(end()); }

template<typename TYPE>
typename List<TYPE>::reverse_iterator List<TYPE>::rend() noexcept { return reverse_iterator(begin()); }

#endif //DATASTRUCTURES_LIST_H
