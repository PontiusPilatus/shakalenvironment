//
// Created by Egor Kashirin on 2019-08-18.
//

#include <gtest/gtest.h>
#include "ping_test.cpp"
#include "ListTests.cpp"

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}