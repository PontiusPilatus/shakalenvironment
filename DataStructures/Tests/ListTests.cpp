//
// Created by Egor Kashirin on 2019-08-19.
//

#include <gtest/gtest.h>
#include "List.h"
#include "stdexcept"

//=============================
//      List Data Management
//=============================

TEST(List, Empty_Constructor) {
    List<int> int_list;
    ASSERT_EQ(0, int_list.size());
}

TEST(List, Empty_Head) {
    List<int> int_list;
    try {
        int_list.top();
        FAIL() << "Expected out of range exception";
    } catch (std::out_of_range const& e) {
        EXPECT_EQ(e.what(),std::string("Head is empty."));
    } catch (...) {
        FAIL() << "Throw invalid exception";
    }
}

TEST(List, Push_Items_back) {
    List<int> expected_list = {1, 2, 3, 4, 5};
    List<int> actual_list;
    for (size_t i = 0; i < 5; ++i) {
        actual_list.push_back(i+1);
    }

    EXPECT_EQ(expected_list, actual_list);
}

TEST(List, Push_Items_By_Index) {
    List<int> expected_list = {8, 1, 6, 2, 3, 7, 4, 5, 9};

    List<int> actual_list;
    for (size_t i = 0; i < 5; ++i) {
        actual_list.push_back(i+1);
    }

    actual_list.push(1, 6);
    actual_list.push(4, 7);
    actual_list.push(0, 8);
    actual_list.push(actual_list.size(), 9);

    EXPECT_EQ(expected_list, actual_list);
}

TEST(List, Remove_Items_By_Index) {
    List<int> expected_list = {2, 4};

    List<int> actual_list;
    for (size_t i = 0; i < 5; ++i) {
        actual_list.push_back(i+1);
    }

    actual_list.remove(2);
    actual_list.remove(0);
    actual_list.remove(actual_list.size()-1);

    EXPECT_EQ(expected_list, actual_list);
}

TEST(List, Try_Access_Index) {
    List<int> list = {1, 2, 4};
    try {
        list[5];
    } catch (std::out_of_range const& e) {
        std::string message = e.what();
        ASSERT_EQ("Index can't be grater than size of the container...", message);
    } catch (...) {
        FAIL() << "Didn't catch expected error";
    }

}

//=============================
//      List Const Iterator
//=============================

TEST(List_Iterator, Const_Create_Iterator) {
    List<int> const list = {1, 2, 3, 4};
    List<int>::const_iterator it = list.begin();
    ASSERT_EQ(list.top(), *it);
}

TEST(List_Iterator, Const_Create_End_Iterator) {
    List<uint8_t> const list = {1, 2, 3, 4};
    List<uint8_t>::const_iterator it = list.end();
    ASSERT_EQ(list[3], *it);
}

TEST(List_Iterator, Const_Forward_Step_Prefix) {
    List<double> const list = {1.0, 2.0, 3.0, 4.0};
    List<double>::const_iterator it = list.begin();
    ASSERT_EQ(list[1], *(++it));
}

TEST(List_Iterator, Const_Forward_Step_Postfix) {
    List<double> const list = {1.0, 2.0, 3.0, 4.0};
    List<double>::const_iterator it = list.begin();
    it++;
    ASSERT_EQ(list[1], *it);
}

TEST(List_Iterator, Const_Backward_Step_Prefix) {
    List<char> const list = {'a', 'b', 'c', 'd'};
    List<char>::const_iterator it = list.begin();
    ++it;++it;
    ASSERT_EQ(list[1], *(--it));
}

TEST(List_Iterator, Const_Backward_Step_Postfix) {
    List<char> const list = {'a', 'b', 'c', 'd'};
    List<char>::const_iterator it = list.begin();
    ++it;++it;
    ASSERT_EQ(list[1], *(--it));
}

TEST(List_Iterator, Const_Equality) {
    List<char> const list = {'a', 'b', 'c', 'd'};
    List<char>::const_iterator it1 = list.begin();
    List<char>::const_iterator it2 = list.begin();
    ASSERT_EQ(it1, it2);
}

TEST(List_Iterator, Const_Iterations_Forward) {
    List<uint8_t> const list = {1, 2, 3, 4};
    auto start = list.begin();
    auto end   = list.end();
    for (; start != end; ++start);
    ASSERT_EQ(end, start);
}

TEST(List_Iterator, Const_Iterations_Backward) {
    List<uint8_t> const list = {1, 2, 3, 4};
    auto start = list.begin();
    auto end   = list.end();
    for (; end != start; --end);
    ASSERT_EQ(start, end);
}

//=============================
//      List Iterator
//=============================

TEST(List_Iterator, Create_Iterator) {
    List<int> list = {1, 2, 3, 4};
    List<int>::iterator it = list.begin();
    ASSERT_EQ(list.top(), *it);
}

TEST(List_Iterator, Create_End_Iterator) {
    List<uint8_t> list = {1, 2, 3, 4};
    List<uint8_t>::iterator it = list.end();
    ASSERT_EQ(list[3], *it);
}

TEST(List_Iterator, Forward_Step_Prefix) {
    List<double> list = {1.0, 2.0, 3.0, 4.0};
    List<double>::iterator it = list.begin();
    ASSERT_EQ(list[1], *(++it));
}

TEST(List_Iterator, Forward_Step_Postfix) {
    List<double> list = {1.0, 2.0, 3.0, 4.0};
    List<double>::iterator it = list.begin();
    it++;
    ASSERT_EQ(list[1], *it);
}

TEST(List_Iterator, Backward_Step_Prefix) {
    List<char> list = {'a', 'b', 'c', 'd'};
    List<char>::iterator it = list.begin();
    ++it;++it;
    ASSERT_EQ(list[1], *(--it));
}

TEST(List_Iterator, Backward_Step_Postfix) {
    List<char> list = {'a', 'b', 'c', 'd'};
    List<char>::iterator it = list.begin();
    ++it;++it;
    ASSERT_EQ(list[1], *(--it));
}

TEST(List_Iterator, Equality) {
    List<char> list = {'a', 'b', 'c', 'd'};
    List<char>::iterator it1 = list.begin();
    List<char>::iterator it2 = list.begin();
    ASSERT_EQ(it1, it2);
}

TEST(List_Iterator, Iterations_Forward) {
    List<uint8_t> const list = {1, 2, 3, 4};
    auto start = list.begin();
    auto end   = list.end();
    for (; start != end; ++start);
    ASSERT_EQ(end, start);
}

TEST(List_Iterator, Iterations_Backward) {
    List<uint8_t> const list = {1, 2, 3, 4};
    auto start = list.begin();
    auto end = list.end();
    for (; end != start; --end);
    ASSERT_EQ(start, end);
}