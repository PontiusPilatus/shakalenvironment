cmake_minimum_required(VERSION 3.14)
project(Eco-System)

set(CMAKE_CXX_STANDARD 17)

# Download and unpack googletest at configure time
configure_file(GoogleTestCMake.txt.in GoogleTests/CMakeLists.txt)
execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
  RESULT_VARIABLE result
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/GoogleTests )

execute_process(COMMAND ${CMAKE_COMMAND} --build .
  RESULT_VARIABLE result
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/GoogleTests )

# Prevent overriding the parent project's compiler/linker
# settings on Windows
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)

# Add googletest directly to our build. This defines
# the gtest and gtest_main targets.
add_subdirectory(${CMAKE_CURRENT_BINARY_DIR}/GoogleTests/googletest-src
        ${CMAKE_CURRENT_BINARY_DIR}/GoogleTests/googletest-build
        EXCLUDE_FROM_ALL)
# The gtest/gtest_main targets carry header search path
# dependencies automatically when using CMake 2.8.11 or
# later. Otherwise we have to add them here ourselves.
if (CMAKE_VERSION VERSION_LESS 2.8.11)
    include_directories("${gtest_SOURCE_DIR}/GoogleTests/include")
endif()

add_subdirectory(${CMAKE_CURRENT_BINARY_DIR}/GoogleTests)
add_subdirectory(DataStructures)
add_subdirectory(Main)