cmake_minimum_required(VERSION 3.14)
project(runnable)

set(CMAKE_CXX_STANDARD 17)

# Add version
set (${PROJECT_NAME}_MAJOR 1)
set (${PROJECT_NAME}_MINOR 0)
set (${PROJECT_NAME}_PATCH 0)
set (${PROJECT_NAME}_TWEAK 0)
set (${PROJECT_NAME}_VERSION "${${PROJECT_NAME}_MAJOR}.${${PROJECT_NAME}_MINOR}.${${PROJECT_NAME}_PATCH}.${${PROJECT_NAME}_TWEAK}")

# Create executable
file(GLOB CPP_SOURCES "src/*.cpp")
add_executable(${PROJECT_NAME} ${CPP_SOURCES} main.cpp)

#link_directories(${CMAKE_SOURCE_DIR}/libs/shared)
link_directories(${CMAKE_CURRENT_BINARY_DIR}/DataStructures/)

# Link shared libraries
target_link_libraries(${PROJECT_NAME} shakal::Data_Structures)

